<?php

namespace Drupal\copyright\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'Copyright' Block.
 *
 * @Block(
 *   id = "copyright_block",
 *   admin_label = @Translation("Copyright block"),
 *   category = @Translation("Copyright Module"),
 * )
 */
class CopyrightBlock extends BlockBase implements BlockPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();

    $form['copyright_block_custom'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Enter custom copyright text or company name here. (defaults to site name)'),
      '#default_value' => isset($config['copyright_block_custom']) ? $config['copyright_block_custom'] : '',
    ];

    $form['copyright_block_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('UNANIMOUS site link (defaults to https://beunanimous.com)'),
      '#default_value' => isset($config['copyright_block_url']) ? $config['copyright_block_url'] : '',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);
    $values = $form_state->getValues();
    $this->configuration['copyright_block_custom'] = $values['copyright_block_custom'];
    $this->configuration['copyright_block_url'] = $values['copyright_block_url'];
  }

  /**
   * {@inheritdoc}
   */  
  public function build() {
    $config = $this->getConfiguration();
    $year = date('Y');
    $siteName = \Drupal::config('system.site')->get('name');

    if (!empty($config['copyright_block_custom'])) {
      $custom = $config['copyright_block_custom'];
    }
    else {
      $custom = $siteName;
    }
    if (!empty($config['copyright_block_url'])) {
      $url = $config['copyright_block_url'];
    }
    else {
      $url = 'https://www.beunanimous.com/';
    }
    return array(
      '#markup' => $this->t('
        <div class="them"><span class="copy">&copy;@year</span> @custom</div>
        <span class="bar">|</span>
        <div class="unan"><a href="@url">Website Design & Development by UNANIMOUS</a></div>', array(
        '@custom' => $custom,
        '@year' => $year,
        '@url' => $url,
      )),
      '#attached' => array(
        'library' => array(
          'copyright/copyright_css',
        ),
      ),
    );
  }

}