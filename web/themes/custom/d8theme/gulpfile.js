/////////////////////////////////////////////////////
// Variables
/////////////////////////////////////////////////////

var gulp = require('gulp');
var minify = require('gulp-minify');
var autoprefixer = require('gulp-autoprefixer');
var jshint = require('gulp-jshint');
var less = require('gulp-less');
var cleanCSS = require('gulp-clean-css');
var plumber = require('gulp-plumber');

// Paths //
var paths = {
  srcCSS: './css/stylesheet.css',
  srcJS: './js/site.js',
  srcLESS: './css/stylesheet.less',
  distCSS: './css',
  distJS: './js',
  subLESS: './css/*/*.less'
};


/////////////////////////////////////////////////////
// Tasks
/////////////////////////////////////////////////////

// Default Task (gulp) //

gulp.task('default', ['watch']);


// Gulp Watch Tasks //

gulp.task('watch' , function() {
	gulp.watch([paths.subLESS , paths.srcLESS], ['less']);
	gulp.watch(paths.srcJS, ['minify-js']);
});


// Compile LESS to CSS, Minify CSS, and Autoprefix //

gulp.task('less', function() {
	return gulp.src(paths.srcLESS)
	.pipe(plumber())
	.pipe(less())
	.pipe(cleanCSS())
	.pipe(autoprefixer({
		browsers: ['last 2 versions'],
		cascade: false
	}))
	.pipe(gulp.dest(paths.distCSS))
});


// Minify JS and run through JS Hint //

gulp.task('minify-js', function() {
	gulp.src(paths.srcJS)
	.pipe(jshint())
	.pipe(jshint.reporter('jshint-stylish'))
	.pipe(minify({
		ext:{
			min:'.min.js'
		},
		exclude: ['tasks'],
		ignoreFiles: ['.combo.js', '-min.js']
	}))
	.pipe(gulp.dest(paths.distJS))
});